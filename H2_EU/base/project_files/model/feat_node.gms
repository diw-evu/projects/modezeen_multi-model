
********************************************************************************
$ontext
The Dispatch and Investment Evaluation Tool with Endogenous Renewables (DIETER).
Version 1.5.0, April 2021.
Written by Alexander Zerrahn, Wolf-Peter Schill, and Fabian St�ckl.
This work is licensed under the MIT License (MIT).
For more information on this license, visit http://opensource.org/licenses/mit-license.php.
Whenever you use this code, please refer to http://www.diw.de/dieter.
We are happy to receive feedback under wschill@diw.de.
$offtext
********************************************************************************

***** Reduce problem size by removing entries of nodes not included in the feat_node parameter  *****

%DSM%$ontext
m_dsm_cu(n,dsm_curt)$(feat_node('dsm',n) = 0) = 0 ;
m_dsm_shift(n,dsm_shift)$(feat_node('dsm',n) = 0) = 0 ;
$ontext
$offtext

%prosumage%$ontext
m_res_pro(n,res)$(feat_node('prosumage',n) = 0) = 0 ;
m_sto_pro_e(n,sto)$(feat_node('prosumage',n) = 0) = 0 ;
m_sto_pro_p(n,sto)$(feat_node('prosumage',n) = 0) = 0 ;
$ontext
$offtext

%heat%$ontext
dh(n,bu,ch,h)$(feat_node('heat',n) = 0) = 0 ;
d_dhw(n,bu,ch,h)$(feat_node('heat',n) = 0) = 0 ;
$ontext
$offtext

Set
map_n_tech(n,tech)
map_n_sto(n,sto)
map_n_rsvr(n,rsvr)
map_n_dsm(n,dsm)
map_n_ev(n,ev)
map_l(l)
map_n_sto_pro(n,sto)
map_n_res_pro(n,tech)
;

map_n_tech(n,tech) = yes$m_p(n,tech) ;
map_n_sto(n,sto) = yes$m_sto_p_in(n,sto) ;
map_n_rsvr(n,rsvr) = yes$m_rsvr_p_out(n,rsvr) ;
map_n_dsm(n,dsm_curt) = yes$m_dsm_cu(n,dsm_curt) ;
map_n_dsm(n,dsm_shift) = yes$m_dsm_shift(n,dsm_shift) ;
map_n_ev(n,ev) = yes$phi_ev(n,ev) ;
map_l(l) = yes$m_ntc(l) ;
map_n_sto_pro(n,sto) = yes$m_sto_pro_p(n,sto) ;
map_n_res_pro(n,res) = yes$m_res_pro(n,res) ;
;

* No interconnections between non-adjacent or nonuploaded nodes
m_ntc(l)$( smax(n,inc(l,n)) = 0 OR smin(n,inc(l,n)) = 0 ) = 0 ;